/**
  ******************************************************************************
  * @file    simcom_api.h
  * @author  SIMCom OpenSDK Team
  * @brief   Header file of all APIs from SIMCom OpenSDK peripherals components.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 SIMCom Wireless.
  * All rights reserved.
  *
  ******************************************************************************
  */
#ifndef _SIMCOM_API_H_
#define _SIMCOM_API_H_

#include "simcom_os.h"
#include "simcom_cfunc.h"
#include "simcom_common.h"
// #include "simcom_debug.h"
// #include "simcom_network.h"
// #include "simcom_simcard.h"
// #include "simcom_sms.h"
// #include "simcom_uart.h"
// #include "simcom_gpio.h"
// #include "simcom_pm.h"
// #include "simcom_i2c.h"
// #include "simcom_spi.h"
// #include "simcom_ntp_client.h"
// #include "simcom_tcpip.h"
// #include "simcom_ftps.h"
// #include "simcom_mqtts_client.h"
// #include "simcom_https.h"
// #include "simcom_ssl.h"
// #include "simcom_system.h"
// #include "simcom_app_download.h"
// #include "simcom_ping.h"
// #include "simcom_call.h"
// #include "simcom_pwm.h"
// #include "simcom_app_updater.h"
// #include "sockets.h"
// #include "simcom_rtc.h"
// #include "simcom_pm.h"
// #include "simcom_usb_vcom.h"
// #include "simcom_cam.h"
// #include "simcom_wtd.h"
// #include "simcom_flash.h"
// #include "simcom_at.h"
// #include "simcom_fota.h"
// #include "simcom_urc.h"



void get_sAPI(void* argv);



#endif
