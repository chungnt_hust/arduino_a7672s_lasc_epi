/*
 * sms.h
 *
 *  Created on: 
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _SMS_H_
#define _SMS_H_


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */
/* Inclusion of system and local header files goes here */
#include "simcom_api.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
/* #define and enum statements go here */
typedef enum
{
    SMS_OK = 0,
    SMS_SEND_FAIL,
    SMS_REV_FAIL,
    SMS_REV_TIMEOUT,
    SMS_SEND_TIMEOUT,
    SMS_NOT_READY,
    SMS_LONG_MSG,
} smsStatus_t;

typedef void (*smsCb)(char *user, char *timestamp, char *data);
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */
/* Definition of public (external) data types go here */
extern sMsgQRef smsUrcMesQ;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */
/* Function prototypes for public (external) functions go here */
void sms_init(void);
void sms_changeStatus(uint8_t stt);
uint8_t sms_isBegin(void);
smsStatus_t sms_sendNewMsg(UINT8 *address, const char *format,...);
void sms_regRxCallback(smsCb _cb);
#endif
#ifdef __cplusplus
}
#endif
